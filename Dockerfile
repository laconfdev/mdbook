FROM rust:1.26.0-slim-jessie AS BUILD

RUN cargo install mdbook --vers 0.1.7


FROM debian:jessie-slim

COPY --from=BUILD /usr/local/cargo/bin/mdbook /bin/mdbook
